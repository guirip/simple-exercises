package fr.rimk.exercices;

/**
 * Simple exercise : The goal is to determine if a word is a palindrome.
 */
public class Palindrome {

    /**
     * Launcher
     * @param args
     */
    public static void main(String[] args) {
	String[] words = { "ROTOR", "RIMK", "SOS", "SDFSDF" };
	for (String word : words){
	    System.out.println(word + " is a palindrome ? " + isPalindrome(word));
	}
    }
    
    /**
     * 
     * @param word
     * @return
     */
    private static boolean isPalindrome(String word){
	boolean value = true;
	int i = 0;
	while (value && i <= word.length() / 2){
	    value = value && (word.charAt(i) == word.charAt(word.length()-(i+1)));
	    i++;
	}
	return value;
    }
}
