package fr.rimk.exercices.alienInvasion;

import java.util.Random;

/**
 * Simple exercise : <br/>
 * On each iteration, a decreasing number of aliens are approching, each at a random distance.<br/>
 * On each iteration the closest alien must be destroyed.
 */
public class AlienInvasion {

    /**
     * Launcher
     */
    public static void main(String[] args) {
	int alienCount = new Random().nextInt(10);

	while (alienCount > 0){
	    SonarData data = SonarData.getRandomData(alienCount);
	    data.log();

	    data.sortAliensByDistance();
	    System.out.println(">> Shooting at " + data.getAliens().get(0).getName());

	    alienCount--;
	}
	System.out.println("\nWar is over.");
    }
}
