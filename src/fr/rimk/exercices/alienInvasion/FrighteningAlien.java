package fr.rimk.exercices.alienInvasion;

public class FrighteningAlien implements Comparable<FrighteningAlien> {

    String name;
    int distance;
    
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public int getDistance() {
        return distance;
    }
    public void setDistance(int distance) {
        this.distance = distance;
    }

    /**
     * Override toString method
     */
    public String toString(){
	return "Alien " + this.getName() + " is at " + this.getDistance() + " km.";
    }

    @Override
    public int compareTo(FrighteningAlien o) {
	return o.getDistance() > this.getDistance() ? -1
		: (o.getDistance() < this.getDistance() ? 1 : 0);
    }
}
