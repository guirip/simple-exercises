package fr.rimk.exercices.alienInvasion;

import java.util.Random;

public class AlienNameHelper {

    private static final char[] VOYELLES = {'A', 'E', 'I', 'O', 'U', 'Y'};
    private static final char[] CONSONNES = {
	'B', 'C', 'D', 'F', 'G', 'H',
	'J', 'K', 'L', 'M', 'N', 'P',
	'Q', 'R', 'S', 'T', 'V', 'W',
	'X', 'Z' };

    /**
     * 
     * @param length
     * @return
     */
    public static String getRandomName(int length){
	Random random = new Random();
	char[] name = new char[length];
	for (int i=0; i<length; i++){
	    name[i] = (i%2 == 0 ? CONSONNES[random.nextInt(CONSONNES.length-1)] : VOYELLES[random.nextInt(VOYELLES.length-1)]);
	}
	return new String(name);
    }
    
}
