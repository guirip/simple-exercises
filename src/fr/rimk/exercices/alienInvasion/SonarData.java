package fr.rimk.exercices.alienInvasion;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class SonarData {

    int aliensCount;
    List<FrighteningAlien> aliens;

    /**
     * Constructor
     */
    public SonarData(){
	aliens = new ArrayList<FrighteningAlien>();
	
    }

    public int getAliensCount() {
        return aliensCount;
    }
    public void setAliensCount(int aliensCount) {
        this.aliensCount = aliensCount;
    }
    public List<FrighteningAlien> getAliens() {
        return aliens;
    }
    public void setAliens(List<FrighteningAlien> aliens) {
        this.aliens = aliens;
    }

    /**
     * 
     * @param number
     * @return
     */
    public static SonarData getRandomData(int number){
	SonarData data = new SonarData();
	data.setAliensCount(number);
	
	for (int i=0 ; i < data.getAliensCount() ; i++){
	    FrighteningAlien alien = new FrighteningAlien();
	    alien.setName(AlienNameHelper.getRandomName(7));
	    alien.setDistance(Math.abs(new Random().nextInt(80)));
	    data.getAliens().add(alien);
	}
	return data;
    }

    /**
     * Override toString method
     */
    public void log(){
	System.out.println("\nNumber of aliens : " + this.getAliensCount());
	for (FrighteningAlien alien : this.getAliens()){
	    System.out.println(alien.toString());
	}
    }

    /**
     * Sort aliens by distance
     */
    public void sortAliensByDistance() {
	Collections.sort(this.getAliens());
    }
}
